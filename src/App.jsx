import {
  Route,
  createBrowserRouter,
  createRoutesFromElements,
  RouterProvider,
} from "react-router-dom";
import { useState, createContext } from "react";
import Home from "./pages/Home";
import Detail from "./pages/Detail";
import NotFoundPage from "./pages/NotFoundPage";
import Header from "./components/Header";

export const ThemeContext = createContext(null);

const router = createBrowserRouter(
  createRoutesFromElements(
    <>
      <Route path="/" element={<Home />} />
      <Route path="/country/:id" element={<Detail />} />
      <Route path="*" element={<NotFoundPage />} />
    </>
  )
);

const App = () => {
  const [darkClass, setDarkClass] = useState("");
  const toggleDarkMode = () => {
    let classValue = darkClass ? "" : " bg-slate-800 text-white ";
    setDarkClass(classValue);
  };
  return (
    <ThemeContext.Provider value={darkClass}>
      <Header toggleDarkMode={toggleDarkMode} />
      <RouterProvider router={router} />
    </ThemeContext.Provider>
  );
};

export default App;
