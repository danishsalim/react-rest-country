import React from "react";
import CountryDetail from "./CountryDetail";

const CountryDetailLayout = ({ children }) => {
  return (
    <>
      <CountryDetail />
    </>
  );
};

export default CountryDetailLayout;
