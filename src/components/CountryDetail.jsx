import React, { useState } from "react";
import { useEffect } from "react";
import { useParams } from "react-router-dom";
import { Link } from "react-router-dom";
import Spinner from "./Spinner";
import NotFoundPage from "../pages/NotFoundPage";

const url = "https://restcountries.com/v3.1/all";

const CountryDetail = () => {
  const [countriesData, setCountriesData] = useState([]);
  const [loading, setLoading] = useState(true);
  const { id } = useParams();

  useEffect(() => {
    const fetchCountryData = async () => {
      try {
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          setLoading(false);
          setCountriesData(data);
        } else {
          throw new Error("error in fetching data");
        }
      } catch (error) {
        setLoading(false);
        console.log(error);
      }
    };
    fetchCountryData();
  }, []);

  if (
    (!loading && countriesData.length == 0) ||
    countriesData.filter((country) => country.ccn3 == id).length == 0
  ) {
    return <NotFoundPage />;
  }

  const country = countriesData.length
    ? countriesData.filter((country) => country.ccn3 == id)[0]
    : {};

  const languages = country.languages
    ? Object.keys(country.languages).map((languageKey) => {
        return country.languages[languageKey];
      })
    : [];

  const nativeName = country.name?.nativeName
    ? Object.keys(country.name.nativeName).map((native) => {
        return country.name.nativeName[native].common;
      })
    : [];

  const currencies = country.currencies
    ? Object.keys(country.currencies).map((currencyKey) => {
        return country.currencies[currencyKey]["name"];
      })
    : [];

  const borders = country.borders
    ? country.borders
        .map((borderCode) => {
          let borderCountry = countriesData.find(
            (country) => country.cca3 === borderCode
          );
          return borderCountry
            ? [borderCountry.name.common, borderCountry.ccn3]
            : null;
        })
        .filter((name) => name !== null)
    : [];

  return (
    <>
      {loading ? (
        <Spinner />
      ) : (
        <div className="p-8 ">
          <Link to={"/"}>
            <button className="shadow-md  px-4 mb-8 ">Back</button>
          </Link>

          <div className="flex flex-col justify-between items-between  gap-y-8  text-sm font-sans w-full ">
            <div className="flex ">
              <img src={country.flags.png} alt="flag" className="h-64 w-80" />
              <div className="detail-container ml-10 flex flex-col  items-between justify-center  w-full  ">
                <h1 className="text-xl font-bold pb-4">
                  {country.name.common}
                </h1>
                <div className="text-container flex flex-col flex-wrap h-32 items-between ">
                  <p className="text-sm ">
                    Native Name:
                    <span className="text-xs font-light ml-2">
                      {nativeName}
                    </span>
                  </p>
                  <p className="text-sm ">
                    Population:
                    <span className="text-xs font-light ml-2">
                      {country.population}
                    </span>
                  </p>
                  <p className="text-sm ">
                    Region:
                    <span className="text-xs font-light ml-2">
                      {country.region}
                    </span>
                  </p>
                  <p className="text-sm ">
                    Sub Region:
                    <span className="text-xs font-light ml-2">
                      {country.subregion}
                    </span>
                  </p>
                  <p className="text-sm ">
                    Capital:
                    <span className="text-xs font-light ml-2">
                      {country.capital}
                    </span>
                  </p>
                  <p className="text-sm ">
                    Top Level Domain:
                    <span className="text-xs font-light ml-2">
                      {country.cca2}
                    </span>
                  </p>
                  <p className="text-sm ">
                    Currencies:
                    <span className="text-xs font-light ml-2">
                      {currencies}
                    </span>
                  </p>
                  <p className="text-sm ">
                    Languages:
                    <span className="text-xs font-light ml-2">{languages}</span>
                  </p>
                </div>
                <div className="border-countries-container mt-2"></div>
                <p className="text-sm ">
                  Border Countries :
                  {borders.length ? (
                    borders.map((borderCountry) => (
                      <Link
                        to={"/country/" + borderCountry[1]}
                        key={borderCountry[1]}
                      >
                        <button className="shadow mx-2 px-4 text-xs font-light ">
                          {borderCountry[0]}
                        </button>
                      </Link>
                    ))
                  ) : (
                    <span className="text-xs font-light ml-2">
                      {" "}
                      No border country available !!
                    </span>
                  )}
                </p>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default CountryDetail;
