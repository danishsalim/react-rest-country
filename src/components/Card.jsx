import React from "react";

const Card = ({ name, population, region, capital, imageSource, area }) => {
  return (
    <div className="cards shadow-lg w-52 shadow-lg">
      <img
        src={imageSource}
        alt={name + "flag"}
        className="card-img w-full h-32"
      />
      <div className="text-container p-4">
        <h2 className="font-medium font-sans mb-2">{name}</h2>
        <p className="text-sm leading-6">
          Population:
          <span className="text-xs font-light ml-2">{population}</span>{" "}
        </p>
        <p className="text-sm leading-6">
          Region:<span className="text-xs font-light ml-2">{region}</span>
        </p>
        <p className="text-sm leading-6">
          Capital:<span className="text-xs font-light ml-2">{capital}</span>
        </p>
        <p className="text-sm leading-6">
          Area:<span className="text-xs font-light ml-2">{area}</span>
        </p>
      </div>
    </div>
  );
};

export default Card;
