import React from "react";
import { CiSearch } from "react-icons/ci";
import { useContext } from "react";
import { ThemeContext } from "../App";

const SearchAndFilter = ({
  countriesData,
  handleFilter,
  selectedRegion,
  handleSubFilter,
  handleSearched,
  handleSortByPopulation,
  handleSortByArea,
}) => {
  const darkClass = useContext(ThemeContext);
  let regions = [...new Set(countriesData.map((country) => country.region))];
  let subregions = [
    ...new Set(
      countriesData
        .filter((country) => country.region == selectedRegion)
        .map((country) => country.subregion)
        .filter((country) => country)
    ),
  ];

  const handleFilterRegion = (event) => {
    let region = event.target.value;
    handleFilter(region);
  };

  const handleFilterSubregion = (event) => {
    let subregion = event.target.value;
    handleSubFilter(subregion);
  };

  const handleSearchCountry = (event) => {
    handleSearched(event.target.value);
  };

  const handlePopulationOrder = (event) => {
    handleSortByPopulation(event.target.value);
  };

  const handleAreaOrder = (event) => {
    handleSortByArea(event.target.value);
  };

  return (
    <div
      className={`flex justify-between items-center px-8 py-4  ${darkClass}`}
    >
      <section className="shadow-md pl-4 flex justify-center items-center">
        <CiSearch />
        <input
          type="text"
          placeholder="Search for a country"
          className={`search-box pl-2 pr-2 border-hidden ${darkClass}`}
          onChange={(event) => handleSearchCountry(event)}
        />
      </section>

      <select
        name="sortByPopulation"
        id=""
        className={`select-region text-xs p-2 shadow-md ${darkClass}`}
        onChange={(event) => handlePopulationOrder(event)}
        value=""
      >
        <option value="" default hidden>
          Sort By Population
        </option>
        <option value="Ascending">Ascending</option>
        <option value="Descending">Descending</option>
      </select>

      <select
        name="sortByArea"
        id=""
        className={`select-region text-xs p-2 shadow-md ${darkClass}`}
        onChange={(event) => handleAreaOrder(event)}
        value=""
      >
        <option value="" default hidden>
          Sort By Area
        </option>
        <option value="Ascending">Ascending</option>
        <option value="Descending">Descending</option>
      </select>

      <select
        name="regions"
        id=""
        className={`select-region text-xs p-2 shadow-md ${darkClass}`}
        onChange={(event) => handleFilterRegion(event)}
      >
        <option value="" default hidden>
          Filter by Region
        </option>
        {regions.map((region) => (
          <option key={region}>{region}</option>
        ))}
      </select>

      <select
        name="subregions"
        id=""
        className={`select-region text-xs p-2 shadow-md ${darkClass}`}
        onChange={(event) => handleFilterSubregion(event)}
      >
        <option value="" default hidden>
          Filter by SubRegion
        </option>
        {subregions.map((subregion) => (
          <option key={subregion}>{subregion}</option>
        ))}
      </select>
    </div>
  );
};

export default SearchAndFilter;
