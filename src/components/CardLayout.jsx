import React from "react";
import Card from "./Card";
import { useContext } from "react";
import { ThemeContext } from "../App";
import { Link } from "react-router-dom";

const CardLayout = ({ countries, loading }) => {
  const darkClass = useContext(ThemeContext);
  return (
    <div
      className={`flex flex-row justify-stretch items-start flex-wrap gap-x-14  gap-y-4 px-8  py-4 ${darkClass}`}
    >
      {!loading && countries.length ? (
        countries.map((country) => (
          <Link to={"/country/" + country.ccn3} key={country.name.common}>
            <Card
              name={country.name.common}
              population={country.population}
              region={country.region}
              capital={country.capital}
              imageSource={country.flags.png}
              area={country.area}
            />
          </Link>
        ))
      ) : (
        <div className="flex flex-row  justify-center   items-center w-full h-64">
          No Such Country Found
        </div>
      )}
    </div>
  );
};

export default CardLayout;
