import React, { useContext, useState } from "react";
import { CiDark } from "react-icons/ci";
import { ThemeContext } from "../App";

const Header = ({ toggleDarkMode }) => {
  const darkClass = useContext(ThemeContext);

  return (
    <>
      <div
        className={`flex justify-between items-center px-8 py-4 border-b-2 border-gray-200 shadow ${darkClass}`}
      >
        <h1>Where in the world?</h1>
        <div className="dark flex   items-center">
          <CiDark />{" "}
          <button onClick={() => toggleDarkMode()}> Dark Mode </button>
        </div>
      </div>
    </>
  );
};

export default Header;
