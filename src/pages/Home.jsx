import { useEffect, useState } from "react";
import CardLayout from "../components/CardLayout";
import SearchAndFilter from "../components/SearchAndFilter";
import Spinner from "../components/Spinner";

const url = "https://restcountries.com/v3.1/all";

const Home = () => {
  const [countriesData, setCountriesData] = useState([]);
  const [loading, setLoading] = useState(true);
  const [selectedRegion, setSelectedRegion] = useState("");
  const [selectedSubRegion, setSelectedSubRegion] = useState("");
  const [searched, setsearched] = useState("");
  const [populationOrder, setPopulationOrder] = useState("");
  const [areaOrder, setAreaOrder] = useState("");

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch(url);
        const data = await response.json();
        if (response.ok) {
          setCountriesData(data);
          setLoading(false);
        } else {
          throw new Error();
        }
      } catch (error) {
        console.log("Error In Fetching ", error);
        setLoading(false);
      }
    };
    fetchData();
  }, []);

  if (!loading && countriesData.length == 0) {
    return (
      <div className="flex flex-col justify-center items-center h-64">
        <p>Error in fetching data</p>
        <p> 404 Not Found !!!</p>
      </div>
    );
  }

  const handleFilter = (selectedRegion) => {
    setSelectedRegion(selectedRegion);
    setSelectedSubRegion("");
  };

  const handleSubFilter = (selectedSubRegion) => {
    setSelectedSubRegion(selectedSubRegion);
  };

  const handleSearched = (searchedData) => {
    setsearched(searchedData);
  };

  const handleSortByPopulation = (order) => {
    setAreaOrder("");
    setPopulationOrder(order);
  };

  const handleSortByArea = (order) => {
    setPopulationOrder("");
    setAreaOrder(order);
  };

  const filteredCountries = countriesData
    .filter((country) => {
      if (!selectedRegion) {
        return true;
      } else if (country.region == selectedRegion) {
        return true;
      } else {
        return false;
      }
    })
    .filter((country) => {
      if (!selectedSubRegion) {
        return true;
      } else if (country.subregion == selectedSubRegion) {
        return true;
      } else if (!country.subregion) {
        return true;
      } else {
        return false;
      }
    })
    .filter((country) => {
      if (!searched) {
        return true;
      } else if (
        country.name.common.toLowerCase().includes(searched.toLowerCase())
      ) {
        return true;
      } else {
        return false;
      }
    });

  if (populationOrder == "Ascending") {
    filteredCountries.sort((a, b) => a.population - b.population);
  } else if (populationOrder == "Descending") {
    filteredCountries.sort((a, b) => b.population - a.population);
  } else if (areaOrder == "Ascending") {
    filteredCountries.sort((a, b) => a.area - b.area);
  } else if (areaOrder == "Descending") {
    filteredCountries.sort((a, b) => b.area - a.area);
  }

  return (
    <>
      <SearchAndFilter
        countriesData={countriesData}
        handleFilter={handleFilter}
        selectedRegion={selectedRegion}
        handleSubFilter={handleSubFilter}
        handleSearched={handleSearched}
        handleSortByPopulation={handleSortByPopulation}
        handleSortByArea={handleSortByArea}
      />
      {!loading ? (
        <CardLayout countries={filteredCountries} loading={loading} />
      ) : (
        <Spinner />
      )}
    </>
  );
};

export default Home;
