import React from "react";
import CountryDetailLayout from "../components/CountryDetailLayout";
import { useContext } from "react";
import { ThemeContext } from "../App";
const Detail = () => {
  const darkClass =useContext(ThemeContext)
  return (
    <div className={darkClass + " h-screen "} >
      <CountryDetailLayout />
    </div>
  );
};

export default Detail;
